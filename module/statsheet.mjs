import { module, templates } from './config.mjs';

function getSkipActionPrompt() {
	return (
		(game.settings.get("pf1", "skipActionDialogs") && !game.keyboard.isDown("Shift")) ||
		(!game.settings.get("pf1", "skipActionDialogs") && game.keyboard.isDown("Shift"))
	);
}

Handlebars.registerHelper('koboldworks-uses', function (item) {
	if (Number.isFinite(item.charges) && item.autoDeductCharges)
		return `×${Math.floor(item.charges / item.chargeCost)}`;
	return `×Inf.`;
});

export class StatSheet extends ActorSheet {
	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			classes: ['koboldworks', 'statsheet', 'pf1', 'sheet'],
			template: templates.statsheet,
			width: 600,
			height: 620,
			minWidth: 460,
			minHeight: 520,
			scrollY: ['form'],
			tabs: [
				/*
				{
					navSelector: '.tab-selector',
					contentSelector: '.tab-content',
					initial: 'summary',
				}
				*/
			]
		});
	}

	/**
	 * Feed info to the sheet.
	 */
	async getData() {
		const data = await super.getData();
		//console.log("getData:", data);
		data.system = data.actor.data.data;
		data.config = CONFIG.PF1;

		const speeds = {};
		for (let [key, value] of Object.entries(data.actor.data.data.attributes.speed)) {
			if (value.total > 0) speeds[key] = {
				value: value.total,
				label: key !== 'land' ? `PF1.Speed${key.capitalize()}_Short` : null,
				maneuverability: value.maneuverability
			};
		}

		const skills = {};

		const simplifySkill = (key, label, data) => {
			if (!label) label = CONFIG.PF1.skills[key];
			const rank = data.rank,
				mod = data.mod,
				rt = data.rt;
			if (rt && rank == null) return;
			skills[key] = { key, label, rank, mod, };
		};

		for (let [key, value] of Object.entries(data.actor.data.data.skills)) {
			if (value.rank !== null)
				simplifySkill(key, null, value);

			if (value.subSkills)
				for (let [key2, value2] of Object.entries(value.subSkills)) {
					if (value2.name?.trim().length > 0) // skip unnamed custom skills
						simplifySkill(`${key}.subSkills.${key2}`, value2.name, value2);
				}
		}

		const attacks = {
			melee: [],
			ranged: [],
			maneuver: []
		};

		const allAttacks = data.actor.items.filter(i => i.type === 'attack');
		attacks.ranged = allAttacks.filter(i => ['rwak', 'rsak'].includes(i.data.data.actionType));
		attacks.melee = allAttacks.filter(i => ['mwak', 'msak'].includes(i.data.data.actionType));
		attacks.maneuver = allAttacks.filter(i => ['rcman', 'mcman'].includes(i.data.data.actionType));

		const spells = {
			caster: false,
			attack: [],
			utility: [],
		};

		const allSpells = data.actor.items.filter(i => i.type === 'spell' && i.isCharged);
		spells.attack = allSpells.filter(i => (i.hasDamage && !i.isHealing));
		spells.utility = allSpells.filter(i => !spells.attack.find(i2 => i2.id === i.id));
		spells.caster = (spells.attack.length || spells.utility.length);

		const resistances = [
			...data.actor.data.data.traits.dr.split(';').map(i => i?.trim()),
			...data.actor.data.data.traits.eres.split(';').map(i => i?.trim())
		].filter(i => i?.length > 0);

		const vulnerabilities = [
			...data.actor.data.data.traits.dv.value, ...data.actor.data.data.traits.dv.custom.split(';').map(i => i?.trim()).filter(i => i?.length > 0)
		].filter(i => i?.length > 0);

		const immunities = [
			...data.actor.data.data.traits.cres.split(';').map(i => i?.trim()),
			...data.actor.data.data.traits.di.value, ...data.actor.data.data.traits.di.custom.split(';').map(i => i?.trim()),
			...data.actor.data.data.traits.ci.value, ...data.actor.data.data.traits.ci.custom.split(';').map(i => i?.trim()),
		].filter(i => i?.length > 0);

		// Usable equipment
		const equipment = data.actor.items.filter(i => i.hasAction && ['equipment', 'consumable'].includes(i.type));

		data.tempData = {
			subTypes: data.actor.race?.data.data.subTypes?.length > 0,
			speeds,
			effectiveHP: data.actor.data.data.attributes.hp.value + data.actor.data.data.attributes.hp.temp,
			resistances,
			vulnerabilities,
			immunities,
			senses: data.actor.data.data.traits.senses.split(';').map(s => s.trim()),
			skills,
			attacks,
			spells,
			equipment,
		};

		//console.log('WIP | StatSheet | getData.data ', data);
		//console.log('WIP | StatSheet | getData.this ', this);

		return data;
	}

	/**
	 * @param {*} html
	 * @override
	 */
	activateListeners(html) {
		super.activateListeners(html);

		const nonNullable = html.find('.no-null');
		nonNullable.change(this._preventNull.bind(this));

		// Enforce minimum size
		html.closest('.window-app').css({ 'min-height': this.options.minHeight, 'min-width': this.options.minWidth });

		const actor = this.actor,
			sheet = this;

		// Nav buttons
		html.find('nav ol li a[data-scroll-to]').on('click', this._scrollTo.bind(this));

		// Register actions
		html.find('action').each(function () {
			const key = this.dataset.key;
			switch (this.dataset.category) {
				case 'skill':
					$(this).on('click', () => actor?.rollSkill(key, { skipDialog: getSkipActionPrompt() }));
					$(this).on('contextmenu', () => sheet._editSkill(this));
					break;
				case 'ability':
					$(this).on('click', () => actor?.rollAbility(key, { skipDialog: getSkipActionPrompt() }));
					break;
				case 'save':
					$(this).on('click', () => actor?.rollSavingThrow(key, { skipDialog: getSkipActionPrompt() }));
					break;
				case 'init':
					$(this).on('click', () => actor?.rollInitiative({ createCombatants: true, rerollInitiative: game.user.isGM }));
					break;
				case 'defenses':
					$(this).on('click', () => actor?.rollDefenses());
					break;
				case 'equipment':
					$(this).on('click', () => actor.items.get(this.dataset.itemId)?.use({ skipDialog: getSkipActionPrompt() }));
					$(this).on('contextmenu', () => actor.items.get(this.dataset.itemId).sheet.render(true));
					break;
				case 'other':
					if (key === 'item') $(this).on('contextmenu', () => actor.items.get(this.dataset.itemId).sheet.render(true));
					break;
				case 'action':
					if (key === 'rest') $(this).on('click', () => {
						const dialog = game.pf1.applications.ActorRestDialog;
						const app = Object.values(actor.apps).find((o) => (o instanceof dialog && o._element));
						if (app) app.bringToTop();
						else new dialog(actor).render(true);
					});
					break;
				case 'attack':
					if (key === 'cmb') $(this).on('click', () => actor?.rollCMB());
					else if (key === 'item') {
						$(this).on('click', () => actor.items.get(this.dataset.itemId)?.useAttack({ skipDialog: getSkipActionPrompt() }));
						$(this).on('contextmenu', () => actor.items.get(this.dataset.itemId).sheet.render(true));
					}
					else {
						console.warn("STATSHEET | UNRECOGNIZED ROLL REGISTRATION:", this);
					}
					break;
				default:
					console.warn("STATSHEET | UNRECOGNIZED ROLL REGISTRATION:", this);
					break;
			}
		});
	}

	_scrollToOffset(offset=0, correction=true) {
		const form = this.element.find('form').get(0);
		if (correction) offset -= form.offsetTop;
		form.scrollTo({ top: Math.max(0, offset) });
	}

	_scrollTo(el) {
		event.preventDefault();
		event.stopPropagation();
		const target = el.target?.dataset?.scrollTo;
		if (!target) return;
		const scrollTo = this.element.find('form').find(`[data-scroll-target=${target}`);
		this._scrollToOffset(scrollTo.get(0).offsetTop);
	}

	/** @override */
	async _render(force, options) {
		await super._render(force, options);

		// Hack to make this._scrollPositions to actually work
		const offset = this._scrollPositions?.form ?? 0
		if (offset > 0) this._scrollToOffset(offset, false);
	}

	async _editSkill(element) {
		const id = element.dataset.key;
		const dialogId = `actor-${this.actor.id}-skill-${id}`;

		// Check for already open dialog
		const oldDialog = Object.values(ui.windows).find(w => w.id === dialogId);
		if (oldDialog?.rendered) {
			oldDialog.maximize();
			oldDialog.bringToTop();
			return;
		}

		const info = this.actor.getSkillInfo(id);
		if (!info) {
			console.warn("Couldn't retrieve skill info for:", id);
			return;
		}

		// Fill in what's missing from getSkillInfo
		const d = getProperty(this.actor.data, `data.skills.${id}`);
		info.ability = d.ability;
		info.cs = d.cs;
		info.acp = d.acp;
		info.actorId = this.actor.id;
		info.skillId = id;

		info.config = { abilities: CONFIG.PF1.abilities };

		const dom = await (new Promise(async (resolve) =>
			await new Dialog(
				{
					title: info.name,
					content: await renderTemplate(templates.dialog.skill, info),
					buttons: {
						save: {
							label: "Save",
							callback: jq => resolve(jq.find('form').first().get(0))
						},
					},
					close: _ => resolve(null),
					default: 'save',
				},
				{
					id: dialogId,
					width: 280,
				}
			).render(true)
		));

		if (!dom) return;

		const nd = {
			ability: dom.elements.ability.value,
			rank: dom.elements.rank.valueAsNumber,
			rt: dom.elements.rt.checked,
			acp: dom.elements.acp.checked
		};

		const updateData = {};
		for (let k of Object.keys(nd))
			if (info[k] !== nd[k])
				updateData[`data.skills.${id}.${k}`] = nd[k];

		if (Object.keys(updateData).length === 0) return;

		return await this.actor.update(updateData);
	}

	_preventNull(event) {
		//console.log('WIP | STATSHEET | _preventNull', event);

		if (event.target.value?.length > 0) return; // OK
		$(event.target).val(event.target.defaultValue || "Invalid");
	}

	/**
	 * @param {*} event
	 * @param {*} data
	 * @override
	 */
	async _updateObject(event, data) {
		//console.log('WIP | SimpleSheet | _updateObject: ', event, data);

		return super._updateObject(event, data);
	}
}
