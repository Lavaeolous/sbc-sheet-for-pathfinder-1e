export const module = 'koboldworks-pf1-sheet';

const getTemplatePath = (file) => `modules/${module}/template/${file}.hbs`;

export const templates = {
	sheet: getTemplatePath('sheet'),
	statsheet: getTemplatePath('statsheet'),
	sub: {
		experience: getTemplatePath('sub/experience'),
		health: getTemplatePath('sub/health'),
		bio: getTemplatePath('sub/bio'),
		inventory: getTemplatePath('sub/inventory'),
		combat: getTemplatePath('sub/combat'),
	},
	dialog: {
		value: getTemplatePath('dialog/value'),
		skill: getTemplatePath('dialog/skill'),
	}
};

Hooks.once('init', () => loadTemplates(Object.values(foundry.utils.flattenObject(templates))));
