import { templates } from '../config.mjs';

export async function getValueDialog({ label, value, hint, type, dtype } = {}) {
	const data = arguments[0];

	return new Promise(async (resolve) =>
		await new Dialog(
			{
				title: "Set new value",
				content: await renderTemplate(templates.dialog.value, data),
				buttons: {
					set: {
						label: "Set",
						callback: jq => resolve(jq.find('input[name="value"]').val())
					},
				},
				close: _ => resolve(undefined),
				default: 'set',
			}
		).render(true)
	);
}
