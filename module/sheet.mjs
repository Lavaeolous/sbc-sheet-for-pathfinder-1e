import { module, templates } from './config.mjs';

export class SimpleSheet extends game.pf1.applications.ActorSheetPF {
	/**
	 * Return HBS template path.
	 */
	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
			classes: ['koboldworks', 'simplesheet', 'pf1', 'sheet'],
			template: templates.sheet,
			width: 520,
			height: 600,
			//minWidth: 740,
			//minHeight: 620,
			tabs: [
				{
					navSelector: '.tab-selector',
					contentSelector: '.tab-content',
					initial: 'summary',
				}
			]
		});
	}

	/**
	 * Feed info to the sheet.
	 */
	async getData() {
		const data = {
			data: await super.getData(),
			derived: {
				items: {

				}
			}
		};

		data.derived.items = data.data.items;

		console.log('WIP | SimpleSheet | getData.data ', data);
		console.log('WIP | SimpleSheet | getData.this ', this);

		return data;
	}

	/**
	 *
	 * @param {*} html
	 * @override
	 */
	activateListeners(html) {
		super.activateListeners(html);

		// Hook rolls
		const rollable = html.find('.rollable');
		rollable.click(this._actorRoll.bind(this));
		// Add Font Awesome icon to them
		//console.log(rollable);
		rollable.prepend($('<i></i>').addClass('fas fa-dice').html('&nbsp;'));

		const nonNullable = html.find('.no-null');
		nonNullable.change(this._preventNull.bind(this));
	}

	_preventNull(event) {
		console.log('WIP | SIMPLESHEET | _preventNull', event);

		if (event.target.value?.length > 0) return; // OK
		$(event.target).val(event.target.defaultValue || "Invalid");
	}

	_actorRoll(event) {
		console.log('WIP | SimpleSheetPF1 | _actorRoll:', event);
		const roll = event.target.dataset.roll ?? event.target.parentNode.dataset.roll;

		function preventDefault() {
			event.preventDefault();
			event.stopPropagation();
		}

		switch (roll) {
			case 'init':
				preventDefault();
				this.actor.rollInitiative({ createCombatants: true, rerollInitiative: game.user.isGM });
				break;
			case 'bab':
				preventDefault();
				this.actor.rollBAB();
				break;
			case 'ref':
				preventDefault();
				this.actor.rollSavingThrow('ref');
				break;
			case 'fort':
				preventDefault();
				this.actor.rollSavingThrow('fort');
				break;
			case 'will':
				preventDefault();
				this.actor.rollSavingThrow('will');
				break;
			default:
				break;
		}
	}

	/**
	 *
	 * @param {*} event
	 * @param {*} data
	 * @override
	 */
	// @ts-ignore
	async _updateObject(event, data) {
		console.log('WIP | SimpleSheet | _updateObject: ', event, data);

		return super._updateObject(event, data);
	}

	_onItemEdit(event) {
		console.log("SimpleSheet | _onItemEdit: ", arguments);
	}

	_onItemSummary(event) {
		console.log("SimpleSheet | _onItemSummary: ", arguments);
	}
}
