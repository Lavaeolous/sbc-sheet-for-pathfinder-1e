import { module } from './module/config.mjs';

Hooks.once('ready', async () => {
	//const SimpleSheet = (await import('./module/sheet.mjs')).SimpleSheet;
	const StatSheet = (await import('./module/statsheet.mjs')).StatSheet;
	//Actors.registerSheet('Koboldworks', SimpleSheet, { types: ['character', 'npc'], makeDefault: false });
	Actors.registerSheet('Koboldworks', StatSheet, { types: ['character', 'npc'], makeDefault: false });
});
