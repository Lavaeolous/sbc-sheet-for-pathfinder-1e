const project = 'kbsheet';

const gulp = require('gulp');
const gulp_less = require('gulp-less');

const lessWatch = ['./less/*.less']; // for watching
const lessOrigin = `./less/${project}.less`; // conversion start
const cssDestination = './css/';// Output

const less = () => gulp.src(lessOrigin).pipe(gulp_less()).pipe(gulp.dest(cssDestination));
gulp.task(less);

function watch(cb) {
	gulp.watch(lessWatch, less);
	cb();
}

gulp.task(watch);

gulp.task('default', gulp.parallel('less'));
